import rclpy
from rclpy.node import Node

from sub_mavlink_control.control import Sub
from sub_control_interfaces.srv import ControlAlive, ControlDepth, ControlState, ControlWrite, ControlWriteDepth, ControlWriteState

class ControlService(Node):
    """
    Service for controlling sub.
    """
    def __init__(self):
        """
        Register services
        """
        super().__init__('control_service')
        self.logger = self.get_logger()
        self.sub = Sub("udp://:14540", self.logger)

        self.control_alive_service = self.create_service(ControlAlive, "control_alive", self.alive)
        self.control_state_service = self.create_service(ControlState, "control_state", self.state)
        self.control_depth_service = self.create_service(ControlDepth, "control_depth", self.depth)
        self.control_write_service = self.create_service(ControlWrite, "control_write", self.write)
        self.control_write_state_service = \
            self.create_service(ControlWriteState, "control_write_state", self.write_state)
        self.control_write_depth_service = \
            self.create_service(ControlWriteDepth, "control_write_depth", self.write_depth)

    def alive(self, request, response):
        response.data: bool = bool(self.sub.alive())
        self.logger.info(f"Received alive request. Alive: {response.data}")
        return response

    def state(self, request, response):
        state: State = self.sub.state()
        self.logger.info(f"Received request for state. State: {state}")
        response.x = float(state.x)
        response.y = float(state.y)
        response.z = float(state.z)
        response.yaw = float(state.yaw)
        response.pitch = float(state.pitch)
        response.roll = float(state.roll)
        return response

    def depth(self, request, response):
        response.depth: float = float(self.sub.depth())
        self.logger.info(f"Received request for depth. Depth: {response.depth}")
        return response

    def write(self, request, response):
        self.logger.info(f"Received request for manual write. Command: '{request.data}'")
        self.atmega.write(request.data)
        return response

    def write_state(self, request, response):
        state: State = State(request.x,
                             request.y,
                             request.z,
                             request.yaw,
                             request.pitch,
                             request.roll)
        self.logger.info(f"Received request for state write. State: '{state}'")

        self.atmega.write_state(state)
        return response

    def write_depth(self, request, response):
        self.logger.info(f"Received request for depth write. Depth: '{request.dist}'")
        self.atmega.write_depth(request.dist)


def main(args=None):
    rclpy.init(args=args)

    control_service = ControlService()
    rclpy.spin(control_service)

    rclpy.shutdown()


if __name__ == '__main__':
    main()
