import mavsdk
import asyncio
from mavsdk.offboard import PositionNedYaw, OffboardError

from sub_mavlink_control.state import State

class Sub:
    """
    MAVLink control module for submarine.
    Uses mavsdk-python to communicate.
    """
    def __init__(self, port, logger):
        self.port = port
        self.logger = logger
        self.sub = mavsdk.System()
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.loop.run_until_complete(self.setup())
        self.target_state = State(0, 0, 0, 0, 0, 0)
        self.current_state = State(0, 0, 0, 0, 0, 0)
        self.depth = 0

    async def setup(self):
        await self.sub.connect(system_address=self.port)

        self.logger.info("-- Arming")
        await self.sub.action.arm()

        await self.sub.offboard.set_position_ned(PositionNedYaw(0.0, 0.0, 0.0, 0.0))
        try:
            await self.sub.offboard.start()
        except OffboardError as error:
            self.logger.error(f"Starting offboard MAVSDK failed with error: {error._result.result}")
            self.logger.error("Disarming")
            await self.sub.action.disarm()
            return

    # Get and set functions (sync)

    def alive(self):
        # TODO: Implement properly
        return True

    def state(self):
        return self.current_state

    def depth(self):
        return self.current_state.z

    def set_relative_state(self, state: State):
        self.loop.run_until_complete(self.__set_relative_state(state))

    def set_state(self, state: State):
        self.loop.run_until_complete(self.__set_state(state))

    def set_depth(self, depth: float):
        # TODO: Implement properly
        self.loop.run_until_complete(self.__set_state(
            self.target_state + State(0, 0, depth, 0, 0, 0)))

    def stop(self):
        self.loop.run_until_complete(self.__stop(state))


    # Asyncio MAVSDK Implementations

    async def __set_relative_state(self, state: State):
        position = PositionNedYaw(self.target_state.x + state.x,
                                  self.target_state.y + state.y,
                                  self.target_state.z + state.z,
                                  state.yaw)
        self.logger.info("Set NEDYaw relative state {position}")
        await self.sub.offboard.set_position_ned(position)
        self.target_state = self.target_state + state

    async def __set_state(self, state: State):
        position = PositionNedYaw(state.x,
                                  state.y,
                                  state.z,
                                  state.yaw)
        self.logger.info("Set NEDYaw absolute state {position}")
        await self.sub.offboard.set_position_ned(position)
        self.target_state = state

    async def __stop(self):
        await self.sub.offboard.stop()

    # Telemetry loops
    async def get_position_ned(self, sub):
        async for pv in sub.telemetry.position_velocity_ned():
            self.current_state.x = pv.position.north_m
            self.current_state.y = pv.position.east_m
            self.current_state.z = pv.position.down_m

    async def get_rotation(self, sub):
        async for rotation in sub.telemetry.attitude_euler():
            self.current_state.yaw = rotation.yaw_deg
            self.current_state.pitch = rotation.pitch_deg
            self.current_state.roll = rotation.roll_deg

    async def get_depth(self, sub):
        async for position in sub.telemetry.position():
            self.depth = position.absolute_altitude_m
