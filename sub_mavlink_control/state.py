from dataclasses import dataclass

@dataclass
class State:
    x: float
    y: float
    z: float
    yaw: float
    pitch: float
    roll: float

    def __add__(self, other_state):
        return State(self.x + other_state.x,
                     self.y + other_state.y,
                     self.z + other_state.z,
                     self.yaw + other_state.yaw,
                     self.pitch + other_state.pitch,
                     self.roll + other_state.roll)
